package com.thisfirehouse.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThisfirehouseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThisfirehouseApplication.class, args);
	}

}
