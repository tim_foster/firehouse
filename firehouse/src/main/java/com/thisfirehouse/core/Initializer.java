package com.thisfirehouse.core;

import com.thisfirehouse.core.model.Firehouse;
import com.thisfirehouse.core.repository.FirehouseRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

@Component
public class Initializer implements CommandLineRunner {
    private final FirehouseRepository repository;

    public Initializer(FirehouseRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... strings) {
        Stream.of("123123", "asd", "asd123").forEach(name ->
                repository.save(new Firehouse(name)));

        repository.findAll().forEach(System.out::println);
    }
}
