package com.thisfirehouse.core.repository;

import com.thisfirehouse.core.model.Firehouse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FirehouseRepository extends JpaRepository<Firehouse, Long> {
    Firehouse findByName(String name);
}
