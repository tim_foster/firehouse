package com.thisfirehouse.core.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
public class Firehouse {
    @Id
    @GeneratedValue
    private Long id;
    @NonNull private String name;
    @NonNull private String description;
    @NonNull private int meetupWeekday; // 1 - 7 (Monday - Sunday)
    @NonNull private int meetupTime;    // Seconds since midnight
    // Address
    private String unitOrBuilding;  // 3 Marne
    private String streetAddress;   // 86 Steenloper street
    private String suburb;          // Monument Park
    private String city;            // Pretoria
    private String stateOrProvince; // Gauteng
    private String country;         // South Africa
    private String postalCode;      // 0105

}