package com.hisfirehouse

class Tag {
    String name
    Integer frequency = 0

    static constraints = {
        name blank: false, nullable: false
        frequency blank: false, nullable: false
    }

    static mapping = {
        frequency defaultValue: "0"
    }
}
