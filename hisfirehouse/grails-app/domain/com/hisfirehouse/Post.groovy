package com.hisfirehouse

class Post {
    String title
    String content
    Integer likes = 0

    static hasMany = [tags: Tag, authors: User]

    static constraints = {
        title blank: false, nullable: false
        content blank: false, nullable: false
        likes blank: false, nullable: false
    }

    static mapping = {
        likes defaultValue: "0"
        content sqlType: "LONGTEXT"
    }
}
