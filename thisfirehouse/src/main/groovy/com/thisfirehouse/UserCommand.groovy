package com.thisfirehouse

import grails.validation.Validateable

class UserCommand implements Validateable {
    private String recaptcha
    private String username
    private String password
    private String displayName

    void setRecaptcha(String recaptcha) { this.recaptcha = recaptcha }

    void setUsername(String username) { this.username = username }

    void setPassword(String password) { this.password = password }

    void setDisplayName(String displayName) { this.displayName = displayName }

    String getRecaptcha() { return this.recaptcha }

    String getUsername() { return this.username }

    String getPassword() { return this.password }

    String getDisplayName() { return this.displayName }

    def getReadableErrors() {
        def errors = []
        if (this.errors.hasFieldErrors("recaptcha"))
            errors.add(new String("Invalid value for recaptcha"))
        if (this.errors.hasFieldErrors("username"))
            errors.add(new String("Invalid value for username"))
        if (this.errors.hasFieldErrors("password"))
            errors.add(new String("Invalid value for password"))
        if (this.errors.hasFieldErrors("displayName"))
            errors.add(new String("Invalid value for displayName"))
        return errors
    }

    static constraints = {
//        recaptcha blank: false, nullable: false
        recaptcha blank: true, nullable: true
        username blank: false, nullable: false, email: true
        password blank: false, nullable: false
        displayName blank: false, nullable: false
    }
}