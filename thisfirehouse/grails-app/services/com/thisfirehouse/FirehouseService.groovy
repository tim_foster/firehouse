package com.thisfirehouse

import grails.gorm.services.Service
import grails.gorm.transactions.Transactional

import java.util.regex.Matcher
import java.util.regex.Pattern

@Transactional
@Service(Firehouse)
abstract class FirehouseService implements IFirehouseService {

    final Pattern firehousePattern = Pattern.compile("^firehouse|^-|[^a-z0-9-]|-\$")

    boolean isPermalinkValid(String permalink) {
        Matcher m = firehousePattern.matcher(permalink)
        return !m.find()
    }

//    String sanitisePermalink(String permalink) {
//        return permalink.toLowerCase().replaceAll(" ", "-")
//    }

    boolean isPermalinkAvailable(String permalink) {
//        permalink = sanitisePermalink(permalink)
//        if (!permalink.startsWith("/burn/")) {
//            permalink = "/burn/" + permalink
//        }
        return !Firehouse.findByPermalink(permalink)
    }
}
