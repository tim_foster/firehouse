package com.thisfirehouse

import grails.gorm.services.Service

interface IFirehouseService {

    Firehouse get(Serializable id)

    List<Firehouse> list(Map args)

    Long count()

    void delete(Serializable id)

    Firehouse save(Firehouse firehouse)

    Firehouse findByPermalink(String permalink)
}
