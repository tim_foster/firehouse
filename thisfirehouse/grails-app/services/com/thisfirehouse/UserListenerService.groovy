package com.thisfirehouse

import grails.compiler.GrailsCompileStatic
import grails.events.annotation.Subscriber
import grails.gorm.transactions.Transactional
import org.grails.datastore.mapping.engine.event.AbstractPersistenceEvent
import org.grails.datastore.mapping.engine.event.PostInsertEvent
import org.springframework.context.ApplicationListener
import org.springframework.security.authentication.event.AuthenticationSuccessEvent
import org.springframework.security.core.userdetails.UserDetails

@Transactional
@GrailsCompileStatic
class UserListenerService implements ApplicationListener<AuthenticationSuccessEvent> {

    IUserService userService

    private static User getUser(AbstractPersistenceEvent event) {
        if (event.entityObject instanceof User) {
            return ((User) event.entityObject)
        }
        null
    }

    @Subscriber
    void preInsert(PostInsertEvent event) {
//        User User = getUser(event)
//        if (User) {
//            emailService.sendActivationEmail(member)
//        }
    }

    @Override
    void onApplicationEvent(AuthenticationSuccessEvent event) {
        UserDetails userDetails = (UserDetails) event.getAuthentication().getPrincipal()
        User authenticatedUser = userService.findByUsername(userDetails.username)
        if (authenticatedUser) {
            authenticatedUser.lastLogin = new Date()
            userService.save(authenticatedUser)
        }
    }
}
