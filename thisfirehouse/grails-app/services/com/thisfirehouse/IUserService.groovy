package com.thisfirehouse

import grails.gorm.services.Service

interface IUserService {

    User get(Serializable id)

    List<User> list(Map args)

    Long count()

    void delete(Serializable id)

    User save(User user)

    User findByUsername(String username)
}
