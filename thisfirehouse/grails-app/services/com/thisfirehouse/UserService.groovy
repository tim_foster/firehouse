package com.thisfirehouse

import grails.gorm.services.Service
import grails.gorm.transactions.Transactional

@Transactional
@Service(User)
abstract class UserService implements IUserService {

    boolean isUsernameAvailable(String username) {
        return !User.findByUsername(username)
    }

    boolean createBasicUser(User user) {
        boolean success = user.save(flush: true)
        // Assign general user role
        Role userRole = Role.findByAuthority("ROLE_EMBER")
        success = success && UserRole.createIfNotExists(user, userRole)
        return success
    }
}
