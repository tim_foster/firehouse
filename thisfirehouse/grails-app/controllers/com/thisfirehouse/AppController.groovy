package com.thisfirehouse

import grails.plugin.springsecurity.annotation.Secured

class AppController {

    @Secured(['permitAll'])
    def robots() {
        render(contentType: 'text/plain', encoding: 'UTF-8', text: "User-Agent: *\nDisallow:\nSitemap: https://hisfirehouse.com/sitemap")
    }

    @Secured(['permitAll'])
    def sitemap() {
        render(contentType: 'text/xml', encoding: 'UTF-8') {
            mkp.yieldUnescaped '<?xml version="1.0" encoding="UTF-8"?>'
            urlset(xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9",
                    'xmlns:xsi': "http://www.w3.org/2001/XMLSchema-instance",
                    'xsi:schemaLocation': "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd") {
                url {
                    loc(g.createLink(absolute: true, controller: 'home', action: 'view'))
                    changefreq('hourly')
                    priority(1.0)
                }
                //more static pages here
//                ...
                //add some dynamic entries
                Firehouse.list().each { domain ->
                    url {
                        loc(g.createLink(absolute: true, controller: 'some', action: 'view', id: domain.id))
                        changefreq('hourly')
                        priority(0.8)
                    }
                }
            }
        }
    }
}
