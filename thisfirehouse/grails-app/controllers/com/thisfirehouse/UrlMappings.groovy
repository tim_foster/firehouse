package com.thisfirehouse

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "/contact"(view:"/contact")
        "/signup"(view:"/signup")
        "/robots.txt" (controller: "app", action: "robots")
        "/robots" (controller: "app", action: "robots")
        "/sitemap.xml" (controller: "app", action: "sitemap")
        "/sitemap" (controller: "app", action: "sitemap")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
