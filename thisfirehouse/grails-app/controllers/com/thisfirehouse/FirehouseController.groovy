package com.thisfirehouse

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

class FirehouseController {

    FirehouseService firehouseService
    SpringSecurityService springSecurityService

    @Secured(['permitAll'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond firehouseService.list(params), model:[firehouseCount: firehouseService.count()]
    }

    @Secured(['permitAll'])
    def create() {
        respond new Firehouse(params)
    }

    @Secured(['permitAll'])
    def show(Long id) {
        respond firehouseService.get(id)
    }

    @Secured(['permitAll'])
    def partialSave() {
        def json = request.JSON
        def result = [:]
        if (!json.firehouseId) {
            result.success = false
            result.message = "Please provide the Firehouse ID"
            return respond(model:result, [formats:['xml', 'json']], status: result.status)
        }

        Firehouse firehouse = firehouseService.get(json.firehouseId)
        if (!firehouse) {
            result.success = false
            result.message = "Please first create a Firehouse and provide its ID"
            return respond(model:result, [formats:['xml', 'json']], status: result.status)
        }

        json.each { key, value ->
            println key + " | " + value
            if (firehouse.hasProperty(key) && !(key in ["dateCreated", "lastUpdated", "likes", "tags", "embers", "stokers", "permalink"])) {
                firehouse[key] = value
            }
        }
        firehouseService.save(firehouse)

        result.success = true
        result.message = "Successfully updated this firehouse"
        result.status = 200
        respond(model:result, [formats:['xml', 'json']], status: result.status)
    }

    @Secured(['permitAll'])
    def initFirehouse() {
        def json = request.JSON
        def result = [:]
        if (!json.permalink) {
            result.success = false
            result.message = "Please provide the Firehouse's permalink"
            return respond(model:result, [formats:['xml', 'json']], status: 400)
        }

        if (!firehouseService.isPermalinkAvailable(json.permalink)) {
            result.success = false
            result.message = "The permalink you have provided already exists"
            return respond(model:result, [formats:['xml', 'json']], status: 400)
        }

        Firehouse firehouse = new Firehouse()
//        firehouse.name = json.name
        firehouse.permalink = firehouseService.sanitisePermalink(json.permalink)
        firehouseService.save(firehouse)

        User user = (User) springSecurityService.getCurrentUser()
        Role stokerRole = Role.findByAuthority("ROLE_STOKER")
        UserRole stoker = UserRole.createIfNotExists(user, stokerRole)
        new FirehouseUserRole(firehouse: firehouse, userRole: stoker).save(flush:true)

        result.success = true
        result.message = "Successfully initialised this firehouse"
        result.firehouseId = firehouse.id
        respond(model:result, [formats:['xml', 'json']], status: 201)
    }

    @Secured(['permitAll'])
    def checkPermalink() {
        def json = request.JSON
        println json
        def result = [:]
        if (!json.permalink) {
            result.success = false
            result.message = "Please provide the Firehouse's permalink"
            return respond(result, status: 400)
        }

        if (!firehouseService.isPermalinkValid(json.permalink)) {
            result.success = false
            result.message = "The permalink you have provided is invalid"
            return respond(result, status: 422)
        }
        else if (!firehouseService.isPermalinkAvailable(json.permalink)) {
            result.success = false
            result.message = "The permalink you have provided already exists"
            return respond(result, status: 422)
        }
        else {
            result.success = true
            result.message = "This permalink is available"
            return respond(result, status: 200)
        }
    }
}
