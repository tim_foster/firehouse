package com.thisfirehouse

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

class UserController {

    UserService userService
    SpringSecurityService springSecurityService

    @Secured(['permitAll'])
    def index() { }

    @Secured(['permitAll'])
    def save(UserCommand user) {
        def result = [:]
        if (user.hasErrors()) {
            result = [success: false, message: user.getReadableErrors(), mode: "ERROR"]
            return respond(model:result, [formats:['xml', 'json']], status: 400)
        }

        if (!userService.isUsernameAvailable(user.username)) {
            result = [success: false, message: "This username is already taken.", mode: "NAME_TAKEN"]
            return respond(model:result, [formats:['xml', 'json']], status: 409)
        }

        User tmpUser = new User(username: user.username, password: user.password, displayName: user.displayName)
        boolean success = userService.createBasicUser(tmpUser)

        if (success)
            result = [success: true, message: "Your account has been created!", mode: "CREATED", status: 201]
        else
            result = [success: false, message: "Fatal server error, please try again later.", mode: "ERROR", status: 500]
        return respond(model:result, [formats:['xml', 'json']], status: result.status)
    }

    @Secured(['ROLE_EMBER'])
    def profile() {
        User user = (User) springSecurityService.getCurrentUser()
        Role stokerRole = Role.findByAuthority("ROLE_STOKER")
        UserRole stokerUserRole = UserRole.findByUserAndRole(user, stokerRole)
        List<Firehouse> firehouses = FirehouseUserRole.findAllByUserRole(stokerUserRole)*.firehouse
        render(view:'profile', model:[user:user, firehouses: firehouses])
    }
}
