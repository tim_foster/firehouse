package com.thisfirehouse

class BootStrap {

    UserService userService
    FirehouseService firehouseService

    def init = { servletContext ->
        User.withNewTransaction {
            // Create Roles
            Role.findOrSaveWhere(authority: "ROLE_ADMIN")
            Role.findOrSaveWhere(authority: "ROLE_EMBER")
            Role.findOrSaveWhere(authority: "ROLE_STOKER")

            User tmpUser = new User(username: "test@test.com", password: "test", displayName: "Test User")
            tmpUser.aboutDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non ipsum eget " +
                    "lacus fringilla volutpat sed non risus. In nec posuere nisl. Etiam mollis lorem non leo dapibus " +
                    "hendrerit. Donec ac fermentum quam. Mauris malesuada magna ut massa gravida pulvinar. Sed non urna" +
                    " dui. Integer suscipit lorem ut massa semper, non mollis massa interdum. Cras ornare enim sed mi " +
                    "dignissim elementum. Sed purus nisl, tincidunt sit amet dolor ut, volutpat mollis metus. Curabitur" +
                    " eu lacinia enim, a blandit elit. Proin tristique tincidunt ornare. Aenean at elementum lectus. " +
                    "Suspendisse potenti. Cras molestie dolor nisl, non hendrerit nulla blandit sed. Aliquam erat volutpat. " +
                    "Praesent at venenatis turpis. Suspendisse leo nisi, porttitor vel accumsan vel, malesuada blandit " +
                    "tortor. Nulla consequat nibh sit amet ipsum sodales lacinia. Nulla ac cursus purus. Vestibulum non " +
                    "turpis feugiat justo consectetur lobortis. Fusce suscipit ipsum id arcu porttitor, vestibulum semper " +
                    "lectus dignissim. Proin in neque aliquam, placerat mauris in, imperdiet erat. Curabitur sit amet lacinia ligula."
            tmpUser.contactNumber = "+27795039328"
            tmpUser.website = "https://mywebsite.com"
            userService.createBasicUser(tmpUser)

            Role stokerRole = Role.findByAuthority("ROLE_STOKER")
            UserRole stoker = UserRole.createIfNotExists(tmpUser, stokerRole)

            Firehouse firehouse = new Firehouse()
            firehouse.permalink = "ignite-pretoria"
            firehouseService.save(firehouse)
            new FirehouseUserRole(firehouse: firehouse, userRole: stoker).save(flush:true)

            Firehouse firehouse2 = new Firehouse()
            firehouse2.permalink = "like the sun"
            firehouseService.save(firehouse2)
            new FirehouseUserRole(firehouse: firehouse2, userRole: stoker).save(flush:true)

        }
    }
    def destroy = {
    }
}
