package com.thisfirehouse

import groovy.transform.ToString

@ToString
class Firehouse {

    String name
    String description
    Date dateCreated
    Date lastUpdated
    String permalink
    Integer likes = 0

    Set<User> getEmbers() {
        getUsersForRole(Role.findByAuthority("ROLE_EMBER"))
    }

    Set<User> getStokers() {
        getUsersForRole(Role.findByAuthority("ROLE_STOKER"))
    }

    private Set<User> getUsersForRole(Role role) {
        List<FirehouseUserRole> rolesForThisFirehouse = FirehouseUserRole.findAllByFirehouse(this)
        List<UserRole> userRoles = (rolesForThisFirehouse.findAll { it.userRole.role == role })*.userRole
        return userRoles*.user as Set<User>
    }

//    static hasMany = [stokers: User, embers: User, tags: Tag]

    void setPermalink(String permalink) {
        this.permalink = permalink.toLowerCase().replaceAll(" ", "-")
    }

    static constraints = {
        likes blank: false, nullable: false
        name blank: false, nullable: true
        permalink blank: false, nullable: false, unique: true
        description blank: false, nullable: true
    }

    static mapping = {
        likes defaultValue: "0"
        description sqlType: "MEDIUMTEXT"
    }
}
