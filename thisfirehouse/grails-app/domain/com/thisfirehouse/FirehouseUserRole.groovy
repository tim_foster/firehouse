package com.thisfirehouse

import groovy.transform.ToString

@ToString
class FirehouseUserRole implements Serializable {

    private static final long serialVersionUID = 1

    Firehouse firehouse
    UserRole userRole

    static constraints = {
        firehouse nullable: false
        userRole nullable: false
    }

    static mapping = {
        id composite: ['firehouse', 'userRole']
        version false
    }
}
