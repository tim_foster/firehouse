package com.thisfirehouse

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
@EqualsAndHashCode(includes='username')
@ToString(includes='username', includeNames=true, includePackage=false)
class User implements Serializable {

    private static final long serialVersionUID = 1

    String username
    String password
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired
    String displayName
    Date dateCreated
    Date lastUpdated
    Date lastLogin
    String aboutDescription
    String contactNumber
    String website

    Set<Role> getAuthorities() {
        (UserRole.findAllByUser(this) as List<UserRole>)*.role as Set<Role>
    }

    static constraints = {
        password nullable: false, blank: false, password: true
        username nullable: false, blank: false, unique: true
        displayName nullable: false, blank: false
        lastLogin nullable: true, blank: false
        aboutDescription nullable: true, blank: false
        contactNumber nullable: true, blank: false
        website nullable: true, blank: false
    }

    static mapping = {
	    password column: '`password`'
        aboutDescription sqlType: "MEDIUMTEXT"
    }
}
