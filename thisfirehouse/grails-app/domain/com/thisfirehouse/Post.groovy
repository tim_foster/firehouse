package com.thisfirehouse

import groovy.transform.ToString

@ToString
class Post {
    String title
    String content
    Integer flames = 0
    User author
    Firehouse firehouse
    Date dateCreated
    Date lastUpdated
    String slug

    static hasMany = [tags: Tag]

    void setTitle(String title) {
        this.title = title
        this.slug = title.toLowerCase().replaceAll(" ", "-")
    }

    static constraints = {
        title blank: false, nullable: false
        content blank: false, nullable: false
        flames blank: false, nullable: false
        author blank: false, nullable: false
        slug blank: false, nullable: false
        firehouse blank: false, nullable: false
    }

    static mapping = {
        flames defaultValue: "0"
        content sqlType: "LONGTEXT"
    }

    String getPermalink(){
        if (title && firehouse) {
            return "/post/" + firehouse.permalink + "/" + title.toLowerCase().replaceAll(" ", "-") + "/" + this.id
        }
        else {
            return "/#"
        }
    }
}
