<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Contact</title>
    <script src="https://www.google.com/recaptcha/api.js?render=6Lcyee8UAAAAAGiECDIlupPVRm0iY9p0Uhf8CejG"></script>
</head>
<body>
<main role="main" class="mt-4 mb-6">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>Contact</h1>

                <div class="card mt-4">
                    <div class="card-header">
                        <h5 class="mb-2 small-heading"><i class="fas fa-envelope-open-text"></i> Mail Us</h5>
%{--                        <p class="mb-0">I <em>do</em> in fact, read our mails. Periodically.</p>--}%
                    </div>
                    <div class="card-body">
                        <form id="contact-form" action="/site/contact" method="post">
                            <input type="hidden" id="recaptcha" name="recaptcha">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" required>
                                <div class="invalid-feedback">Please provide your full name.</div>
                            </div>
                            <div class="form-group">
                                <label for="fromMail">Email address</label>
                                <input type="email" class="form-control" id="fromMail" aria-describedby="emailHelp" name="fromMail" required>
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                <div class="invalid-feedback">Please provide a valid email address so I can mail you back.</div>
                            </div>
                            <div class="form-group">
                                <label for="subject">Subject</label>
                                <input type="text" class="form-control" id="subject" name="subject" required>
                                <div class="invalid-feedback">Please provide a valid subject.</div>
                            </div>
                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea class="form-control" name="message" id="message" rows="4" required></textarea>
                                <div class="invalid-feedback">Please enter a message.</div>
                            </div>

                            <div class="alert alert-info" role="alert">
                                <div class="row">
                                    <div class="col-auto d-flex align-items-center pr-0 pl-1">
                                        <i class="fas fa-info-circle" style="font-size: 2.5em;"></i>
                                    </div>
                                    <div class="col">
                                        <small>Please note that we've implemented reCAPTCHA v3 on this site in order to
                                        avoid spam. Use of this software is subject to the Google
                                        <a href="https://policies.google.com/privacy" target="_blank">Privacy Policy</a>
                                        and <a href="https://policies.google.com/terms" target="_blank">Terms of Use.</a></small>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <button id="sendmail" class="btn btn-primary w-100" type="button">
                                        <span id="send-mail-spinner" class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                                        <i id="send-mail-icon" class="far fa-paper-plane"></i>
                                        <span id="send-mail-status">Send Mail</span>
                                    </button>
                                </div>
                                <div class="col-md-9">
                                    <div class="d-none invalid-feedback" id="fail-form-feedback"></div>
                                    <div class="d-none valid-feedback" id="success-form-feedback"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<content tag="footScripts">
    <script>
        $("#nav-contact").addClass("active");
        $("#nav-contact > a").append(" <span class=\"sr-only\">(current)</span>")
    </script>

    <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('6Lcyee8UAAAAAGiECDIlupPVRm0iY9p0Uhf8CejG', {action: 'contact'}).then(function(token) {
                document.getElementById('recaptcha').value = token;
            });
        });

        var sendMailForm = $("#sendmail");

        sendMailForm.click( function() {
            $("#fail-form-feedback").removeClass("d-inline").addClass("d-none");
            $("#success-form-feedback").removeClass("d-inline").addClass("d-none");
            if (checkInputs()) {
                setSendMailBtnLoading();
                $.ajax({
                    type: "POST",
                    url: "/site/contact",
                    data: $("#contact-form").serialize(),
                    timeout: 10000,
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (textStatus === "timeout") {
                            $("#fail-form-feedback")
                                .text("The server was unresponsive, please try again.")
                                .removeClass("d-none").addClass("d-inline")
                        }
                        else if (textStatus === "error") {
                            if (jqXHR.responseJSON) {
                                var errors = "<strong>Server encountered errors:</strong><br><ul class='pl-3'>";
                                for(var err in jqXHR.responseJSON.message) {
                                    errors += "<li>" + jqXHR.responseJSON.message[err] +"</li>";
                                };
                                errors += "</ul>";
                                $("#fail-form-feedback")
                                    .html(errors)
                                    .removeClass("d-none").addClass("d-inline")
                            } else {
                                $("#fail-form-feedback")
                                    .text("The server encountered an error, please try again.")
                                    .removeClass("d-none").addClass("d-inline")
                            }
                        }
                        setSendMailBtnReady()
                    },
                    success: function (response) {
                        setSendMailBtnReady();
                        $("#success-form-feedback")
                            .text(response.message)
                            .removeClass("d-none").addClass("d-inline")
                    }
                });
            }
        });

        function checkInputs() {
            var isValid = true;
            $(".form-control").filter('[required]').removeClass("is-invalid");
            $('.form-control').filter('[required]').each(function() {
                if ($(this).val().trim().length < 1) {
                    $(this).addClass("is-invalid");
                    isValid = false;
                }
            });
            return isValid;
        }

        function setSendMailBtnLoading() {
            $("#send-mail-spinner").removeClass("d-none");
            $("#send-mail-status").text("Sending...");
            sendMailForm.prop('disabled', true).blur();
            $("#send-mail-icon").addClass("d-none");
        }

        function setSendMailBtnReady() {
            $("#send-mail-spinner").addClass("d-none");
            $("#send-mail-status").text("Send Mail");
            sendMailForm.prop('disabled', false);
            $("#send-mail-icon").removeClass("d-none");
        }
    </script>
</content>
</body>
</html>
