<%@ page import="com.thisfirehouse.User" %>
<%@ page import="com.thisfirehouse.Firehouse" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Profile</title>
</head>
<body>
<main role="main" class="mt-4 mb-6">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="mb-4">Welcome back, ${user?.displayName}</h1>
                <div class="row">
                    <div class="col-2 disable-border border-right">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="true">Profile</a>
                            <a class="nav-link" id="v-pills-your-firehouses-tab" data-toggle="pill" href="#v-pills-your-firehouses" role="tab" aria-controls="v-pills-your-firehouses" aria-selected="false">Your Firehouses</a>
                            <a class="nav-link active" id="v-pills-start-a-firehouse-tab" data-toggle="pill" href="#v-pills-start-a-firehouse" role="tab" aria-controls="v-pills-start-a-firehouse" aria-selected="false">Start a Firehouse</a>
                            <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</a>
                        </div>
                    </div>
                    <div class="col-10">
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <strong>Account Details</strong>
                                            </div>
                                            <div class="card-body">
                                                <form>
                                                    <div class="form-group row mb-0">
                                                        <label for="displayName" class="col-sm-2 col-form-label">Display name</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" readonly class="form-control-plaintext" id="displayName" value="${user?.displayName}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-0">
                                                        <label for="username" class="col-sm-2 col-form-label">Username</label>
                                                        <div class="col-sm-10">
                                                            <input type="email" readonly class="form-control-plaintext" id="username" value="${user?.username}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-0">
                                                        <label for="memberSince" class="col-sm-2 col-form-label">Member since</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" readonly class="form-control-plaintext" id="memberSince" value="${user?.dateCreated}">
                                                        </div>
                                                    </div>
                                                </form>
                                                <hr>
                                                <h5>About You</h5>
                                                <p>
                                                    ${user?.aboutDescription}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <strong>Contact Details</strong>
                                            </div>
                                            <div class="card-body">
                                                <form>
                                                    <div class="form-group row mb-0">
                                                        <label for="emailAddress" class="col-sm-2 col-form-label">Email address</label>
                                                        <div class="col-sm-10">
                                                            <input type="email" readonly class="form-control-plaintext" id="emailAddress" value="${user?.username}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-0">
                                                        <label for="contactNumber" class="col-sm-2 col-form-label">Contact number</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" readonly class="form-control-plaintext" id="contactNumber" value="${user?.contactNumber}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-0">
                                                        <label for="website" class="col-sm-2 col-form-label">Website</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" readonly class="form-control-plaintext" id="website" value="${user?.website}">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-your-firehouses" role="tabpanel" aria-labelledby="v-pills-your-firehouses-tab">
                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <strong>Your Firehouses</strong>
                                            </div>
                                            <div class="card-body">
                                                <g:each var="firehouse" in="${firehouses}">
                                                    <div class="row">
                                                        <div class="col">
                                                            <p>Permalink: ${firehouse.permalink}</p>
                                                            <p>Name: ${firehouse.name}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </g:each>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade show active" id="v-pills-start-a-firehouse" role="tabpanel" aria-labelledby="v-pills-start-a-firehouse-tab">
                                <div class="row">
                                    <div class="col">
                                        <p>Thank you for choosing to ignite a Firehouse. We know it's a huge responsibility to host the presence
                                        of God, so in order to help you focus on what matters, we've created a simple interface for you to organise
                                        and run your Firehouse.</p>

                                        <h3>Let's Begin.</h3>
                                        <p>Please create a permalink for your Firehouse using the button below. This is a unique reference for your
                                        Firehouse and will help you to easily share your page with your friends and family.</p>

                                        <label class="mt-3" for="firehouse-url"><Strong>Your Firehouse's Permalink</Strong></label>
                                        <form>
                                            <div class="form-row">
                                                <div class="col-md-9">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="firehouse-url-prepend">firehouse/</span>
                                                        </div>
                                                        <input type="text" class="form-control" id="firehouse-url" aria-describedby="firehouse-url-prepend">
                                                        <small id="permalinkHelp" class="form-text text-muted">Permalinks may not start with the word "Firehouse" and may not contain special symbols, underscores or quotation marks.</small>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" id="create-permalink" class="btn btn-outline-primary w-100">Create Permalink</button>
                                                </div>
                                            </div>
                                        </form>



                                        <div class="card d-none">
                                            <div class="card-header">
                                                <strong>Create New Firehouse</strong>
                                            </div>
                                            <div class="card-body">
                                                %{--                                                ${firehouses}--}%
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<content tag="footScripts">
    <script>
        // $("#nav-firehouses").addClass("active");
        // $("#nav-firehouses > a").append(" <span class=\"sr-only\">(current)</span>")
    </script>
    <script>

        $(document).ready(function() {
            var debounce = null;
            var permalinkUrl = $("#firehouse-url");
            var permalinkCreateBtn = $("#create-permalink");
            $(permalinkUrl).bind("keyup change", function(e) {
                if (!permalinkCreateBtn.prop("disabled")) {
                    permalinkCreateBtn.prop("disabled", true);
                    permalinkCreateBtn.html(
                        `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Checking...`
                    );
                }
                clearTimeout(debounce);
                debounce = setTimeout(function() {
                    $.ajax({
                        type: "POST",
                        url: "/firehouse/checkPermalink",
                        data: JSON.stringify({ "permalink": permalinkUrl.val() }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        timeout: 5000,
                        error: function (jqXHR, textStatus, errorThrown) {
                            permalinkCreateBtn.prop("disabled", false);
                            permalinkCreateBtn.html("Create Permalink");

                            permalinkCreateBtn.addClass("btn-outline-danger");
                            permalinkCreateBtn.removeClass("btn-outline-success");
                            permalinkCreateBtn.removeClass("btn-outline-primary");

                            // if (textStatus === "timeout") {
                            //     $("#fail-form-feedback")
                            //         .text("The server was unresponsive, please try again.")
                            //         .removeClass("d-none").addClass("d-inline")
                            // }
                            // else if (textStatus === "error") {
                            //     if (jqXHR.responseJSON) {
                            //         var errors = "<strong>Server encountered errors:</strong><br><ul class='pl-3'>";
                            //         for(var err in jqXHR.responseJSON.message) {
                            //             errors += "<li>" + jqXHR.responseJSON.message[err] +"</li>";
                            //         };
                            //         errors += "</ul>";
                            //         $("#fail-form-feedback")
                            //             .html(errors)
                            //             .removeClass("d-none").addClass("d-inline")
                            //     } else {
                            //         $("#fail-form-feedback")
                            //             .text("The server encountered an error, please try again.")
                            //             .removeClass("d-none").addClass("d-inline")
                            //     }
                            // }
                            // setSendMailBtnReady()
                        },
                        success: function (response) {
                            console.log('here')
                            console.log(response)
                            permalinkCreateBtn.prop("disabled", false);
                            permalinkCreateBtn.html("Create Permalink");

                            permalinkCreateBtn.addClass("btn-outline-success");
                            permalinkCreateBtn.removeClass("btn-outline-danger");
                            permalinkCreateBtn.removeClass("btn-outline-primary");

                            // setSendMailBtnReady();
                            // $("#success-form-feedback")
                            //     .text(response.message)
                            //     .removeClass("d-none").addClass("d-inline")
                        }
                    });
                }, 750);
            });
        });

        // sendMailForm.click( function() {
        //     $("#fail-form-feedback").removeClass("d-inline").addClass("d-none");
        //     $("#success-form-feedback").removeClass("d-inline").addClass("d-none");
        //     if (checkInputs()) {
        //         setSendMailBtnLoading();
        //         $.ajax({
        //             type: "POST",
        //             url: "/site/contact",
        //             data: $("#contact-form").serialize(),
        //             timeout: 10000,
        //             error: function (jqXHR, textStatus, errorThrown) {
        //                 if (textStatus === "timeout") {
        //                     $("#fail-form-feedback")
        //                         .text("The server was unresponsive, please try again.")
        //                         .removeClass("d-none").addClass("d-inline")
        //                 }
        //                 else if (textStatus === "error") {
        //                     if (jqXHR.responseJSON) {
        //                         var errors = "<strong>Server encountered errors:</strong><br><ul class='pl-3'>";
        //                         for(var err in jqXHR.responseJSON.message) {
        //                             errors += "<li>" + jqXHR.responseJSON.message[err] +"</li>";
        //                         };
        //                         errors += "</ul>";
        //                         $("#fail-form-feedback")
        //                             .html(errors)
        //                             .removeClass("d-none").addClass("d-inline")
        //                     } else {
        //                         $("#fail-form-feedback")
        //                             .text("The server encountered an error, please try again.")
        //                             .removeClass("d-none").addClass("d-inline")
        //                     }
        //                 }
        //                 setSendMailBtnReady()
        //             },
        //             success: function (response) {
        //                 setSendMailBtnReady();
        //                 $("#success-form-feedback")
        //                     .text(response.message)
        //                     .removeClass("d-none").addClass("d-inline")
        //             }
        //         });
        //     }
        // });
        //
        // function checkInputs() {
        //     var isValid = true;
        //     $(".form-control").filter('[required]').removeClass("is-invalid");
        //     $('.form-control').filter('[required]').each(function() {
        //         if ($(this).val().trim().length < 1) {
        //             $(this).addClass("is-invalid");
        //             isValid = false;
        //         }
        //     });
        //     return isValid;
        // }
        //
        // function setSendMailBtnLoading() {
        //     $("#send-mail-spinner").removeClass("d-none");
        //     $("#send-mail-status").text("Sending...");
        //     sendMailForm.prop('disabled', true).blur();
        //     $("#send-mail-icon").addClass("d-none");
        // }
        //
        // function setSendMailBtnReady() {
        //     $("#send-mail-spinner").addClass("d-none");
        //     $("#send-mail-status").text("Send Mail");
        //     sendMailForm.prop('disabled', false);
        //     $("#send-mail-icon").removeClass("d-none");
        // }
    </script>
</content>
</body>
</html>
