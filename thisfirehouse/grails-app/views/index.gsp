<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Home</title>
</head>
<body>
<main role="main">
%{--    <div id="myCarousel" class="carousel slide" data-ride="carousel">--}%
    <div id="myCarousel" class="carousel slide">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active" style="background-image: url(${assetPath(src: 'antenna-ZDN-G1xBWHY-unsplash.jpg')})">
%{--                <div class="container">--}%
                    <div class="carousel-caption">
                        <div class="row">
                            <div class="col-md-8 col-lg-6 col-xl-4 bg-carousel">
                                <h1>Example headline.</h1>
                                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                                <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
                            </div>
                        </div>
%{--                    </div>--}%
                </div>
            </div>
            <div class="carousel-item" style="background-image: url(${assetPath(src: 'duy-pham-Cecb0_8Hx-o-unsplash.jpg')})">
                <div class="container">
                    <div class="carousel-caption">
                        <div class="row">
                            <div class="col-md-8 col-lg-6 col-xl-4 bg-carousel">
                                <h1>Another example headline.</h1>
                                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                                <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item" style="background-image: url(${assetPath(src: 'ismael-paramo-I-YAoNw2nds-unsplash.jpg')})">
                <div class="container">
                    <div class="carousel-caption">
                        <div class="row d-block">
                            <div class="col-md-8 col-lg-6 col-xl-4 bg-carousel float-right">
                                <h1>One more for good measure.</h1>
                                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                                <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <!-- START THE FEATURETTES -->
    <div class="container">
        <div class="row featurette d-flex">
            <div class="col-md-7">
                <h2 class="featurette-heading">What is a Firehouse? <span class="text-muted">It’ll blow your mind.</span></h2>
                <p class="lead">A group of people who come together to encounter the Holy Spirit and wait on Him like on
                the day of Pentecost. Firehouses provide an ideal setting in which to meet like-minded people who are
                hungry for the presence of God. We give the Holy Spirit free-reign and expect that his fire will fall
                upon us as we wait on Him.</p>
            </div>
            <div class="col-md-5">
                <img src="${assetPath(src: 'worship.jpeg')}" class="img-fluid featurette-image mx-auto rounded" alt="Worship">
            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7 order-md-2">
                <h2 class="featurette-heading">What happens in a Firehouse? <span class="text-muted">See for yourself.</span></h2>
                <p class="lead">A time of fellowship, prayer and encountering the Holy Spirit. Who knows what will
                happen when God's people come together to seek His face? A Firehouse aims to eliminate all the formalities
                found in a church setting and relies soley on the agenda of the Holy Spirit. We wait on Him and let his
                Spirit lead us.</p>
            </div>
            <div class="col-md-5 order-md-1">
                <img src="${assetPath(src: 'prayer.jpeg')}" class="img-fluid featurette-image mx-auto rounded" alt="Prayer">
            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
                <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
            </div>
            <div class="col-md-5">
                <svg class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="500" height="500" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x500"><title>Placeholder</title><rect width="100%" height="100%" fill="#eee"/><text x="50%" y="50%" fill="#aaa" dy=".3em">500x500</text></svg>
            </div>
        </div>
    </div>
    <!-- /END THE FEATURETTES -->

    <div class="breakout-orange text-white mt-6 pt-5 pb-5 text-center">
        <div class="container">
            <h1>Revival is coming.</h1>
            <h3>Join us as we connect His people and become torchbearers of his revival fire</h3>
            <svg class="pb-2 pt-2 rounded bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="800" height="450" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x500"><title>Placeholder</title><rect width="100%" height="100%" fill="#000"/><text x="50%" y="50%" fill="#aaa" dy=".3em">800x450</text></svg>
        </div>
    </div>
    <div class="mt-6 text-center">
        <div class="container">
            <h1>All around the world flames are being ignited by His spirit.</h1>
            <h3>Join the Firehouse community today and become a torch bearer of His revival fire.</h3>
            <img src="${assetPath(src: 'worldmap.jpeg')}" class="img-fluid" alt="World Map">
        </div>
    </div>
    <div class="breakout-blue text-white mt-6 pt-5 pb-5 text-center">
        <div class="container">
            <div class="row mb-2">
                <div class="col">
                    <h1 class="display-3 font-weight-bold">Get Involved</h1>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <button type="button" class="btn btn-dark btn-lg mr-3">Find a Firehouse</button>
                    <button type="button" class="btn btn-dark btn-lg">Ignite a Firehouse</button>
                </div>
            </div>
        </div>
    </div>
</main>

<content tag="footScripts">
    <script>
        $("#nav-home").addClass("active");
        $("#nav-home > a").append(" <span class=\"sr-only\">(current)</span>")
    </script>
</content>
</body>
</html>
