<!doctype html>
<html>
<head>
    <meta name="layout" content="nofooter"/>
    <title>Login</title>
</head>

<body>
    <main role="main" class="cover-body pt-4">
%{--        cover-container d-flex w-100 h-100 p-3 mx-auto flex-column--}%
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-5">
                <div class="card bg-dark text-white">
                    <div class="card-header">
                        <h1 class="mb-4">Welcome back to His Firehouse</h1>
                        <p>Sign into your account below</p>
                    </div>
                    <div class="card-body">
                        <g:if test='${flash.message}'>
                            <div class="login_message">${flash.message}</div>
                        </g:if>

                        <form action="${postUrl ?: '/login/authenticate'}" method="POST" id="loginForm" autocomplete="off">
                            <div class="form-group">
                                <label for="username">Email Address</label>
                                <input type="email" class="form-control" name="${usernameParameter ?: 'username'}" id="username" required />
                                <div class="invalid-feedback">Please provide your email address.</div>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="${passwordParameter ?: 'password'}" id="password" required/>
                                <div class="invalid-feedback">Please enter your password.</div>
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" name="${rememberMeParameter ?: 'remember-me'}" id="remember_me" <g:if test='${hasCookie}'>checked="checked"</g:if>/>
                                <label class="form-check-label" for="remember_me">Remember Me</label>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn float-right btn-primary w-100" id="submit" value="${message(code: 'springSecurity.login.button')}"/>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-center links">
                            Don't have an account?&nbsp;<a href="/signup">Sign Up</a>
                        </div>
                        <div class="d-flex justify-content-center">
                            <a href="#">Forgot your password?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<content tag="footScripts">
    <script>
        $("#nav-login").addClass("active");
        $("#nav-login > a").append(" <span class=\"sr-only\">(current)</span>")
    </script>
    <script>
        (function() {
            document.forms['loginForm'].elements['${usernameParameter ?: 'username'}'].focus();
        })();
    </script>
</content>
</body>
</html>