<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Contact</title>
    <script src="https://www.google.com/recaptcha/api.js?render=6Lcyee8UAAAAAGiECDIlupPVRm0iY9p0Uhf8CejG"></script>
</head>
<body>
<main role="main" class="mt-4">
    <div class="container">
        <div class="row">
            <div class="col-md-5 disable-border border-right">
                <h1 class="mb-4">Join the Firehouse community</h1>
                <ul class="list-unstyled">
                    <li class="media">
                        <i class="fab fa-gripfire mr-3 fa-4x text-center" style="width: 64px"></i>
                        <div class="media-body">
                            <h5 class="mt-0">Start your own Firehouse</h5>
                            Signing up allows you to start and administrate Firehouses in your area.
                        </div>
                    </li>
                    <li class="media my-4">
                        <i class="fas fa-heart mr-3 fa-4x"></i>
                        <div class="media-body">
                            <h5 class="mt-0">Unlock additional functionality</h5>
                            Such as voting, commenting and liking your favourite Firehouses.
                        </div>
                    </li>
                    <li class="media">
                        <i class="fas fa-medal mr-3 fa-4x"></i>
                        <div class="media-body">
                            <h5 class="mt-0">Earn reputation and badges</h5>
                            Signing up allows us to track your rep and achievements with your profile.
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-7">
                <h1 class="mb-4">Create your account</h1>
                <form id="signup-form">
                    <input type="hidden" id="recaptcha" name="recaptcha">
                    <div class="form-group">
                        <label for="displayName">Display Name</label>
                        <input type="text" class="form-control" id="displayName" name="displayName" aria-describedby="nameHelp" placeholder="Your Name or Nickname" required>
                        <small id="nameHelp" class="form-text text-muted">This is what will be shown to other users.</small>
                        <div class="invalid-feedback">Please provide a display name, most people like to use their full name.</div>
                    </div>
                    <div class="form-group">
                        <label for="username">Email Address</label>
                        <input type="email" class="form-control" id="username" name="username" aria-describedby="emailHelp" placeholder="name@example.com" required>
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        <div class="invalid-feedback">Please provide your email address.</div>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" required>
                        <div class="invalid-feedback">Please enter a strong password.</div>
                    </div>
%{--                    <div class="form-group form-check">--}%
%{--                        <input type="checkbox" class="form-check-input" id="exampleCheck1">--}%
%{--                        <label class="form-check-label" for="exampleCheck1">Check me out</label>--}%
%{--                    </div>--}%
                    <div class="alert alert-info" role="alert">
                        <div class="row">
                            <div class="col-auto d-flex align-items-center pr-0 pl-1">
                                <i class="fas fa-info-circle" style="font-size: 2.5em;"></i>
                            </div>
                            <div class="col">
                                <small>Please note that we've implemented reCAPTCHA v3 on this site in order to
                                avoid spam. Use of this software is subject to the Google
                                <a href="https://policies.google.com/privacy" target="_blank">Privacy Policy</a>
                                and <a href="https://policies.google.com/terms" target="_blank">Terms of Use.</a></small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <button id="signup-btn" class="btn btn-primary w-100" type="button">
                                <span id="signup-btn-spinner" class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                                <i id="signup-btn-icon" class="fas fa-sign-in-alt"></i>
                                <span id="send-mail-status">Sign Up!</span>
                            </button>
                        </div>
                        <div class="col-md-9">
                            <div class="d-none invalid-feedback" id="fail-form-feedback"></div>
                            <div class="d-none valid-feedback" id="success-form-feedback"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>

<content tag="footScripts">
    <script>
        $("#nav-signup").addClass("active");
        $("#nav-signup > a").append(" <span class=\"sr-only\">(current)</span>")
    </script>

    <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('6Lcyee8UAAAAAGiECDIlupPVRm0iY9p0Uhf8CejG', {action: 'contact'}).then(function(token) {
                document.getElementById('recaptcha').value = token;
            });
        });

        var signupForm = $("#signup-btn");
        signupForm.click( function() {
            $("#fail-form-feedback").removeClass("d-inline").addClass("d-none");
            $("#success-form-feedback").removeClass("d-inline").addClass("d-none");
            if (checkInputs()) {
                setSendMailBtnLoading();
                $.ajax({
                    type: "POST",
                    url: "/user/save",
                    data: $("#signup-form").serialize(),
                    timeout: 10000,
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (textStatus === "timeout") {
                            $("#fail-form-feedback")
                                .text("The server was unresponsive, please try again.")
                                .removeClass("d-none").addClass("d-inline")
                        }
                        else if (textStatus === "error") {
                            if (jqXHR.responseJSON) {
                                var error = "";
                                if (jqXHR.responseJSON.mode === "ERROR") {
                                    var errors = "<strong>Server encountered errors:</strong><br><ul class='pl-3'>";
                                    for (var err in jqXHR.responseJSON.message) {
                                        errors += "<li>" + jqXHR.responseJSON.message[err] + "</li>";
                                    }
                                    errors += "</ul>";
                                    error = errors;
                                }
                                else {
                                    error = "<strong>This username is already taken.</strong>"
                                }
                                $("#fail-form-feedback")
                                    .html(error)
                                    .removeClass("d-none").addClass("d-inline")
                            } else {
                                $("#fail-form-feedback")
                                    .text("The server encountered an error, please try again.")
                                    .removeClass("d-none").addClass("d-inline")
                            }
                        }
                        setSendMailBtnReady()
                    },
                    success: function (response) {
                        setSendMailBtnReady();
                        $("#success-form-feedback")
                            .text(response.message)
                            .removeClass("d-none").addClass("d-inline")
                    }
                });
            }
        });

        function checkInputs() {
            var isValid = true;
            $(".form-control").filter('[required]').removeClass("is-invalid");
            $('.form-control').filter('[required]').each(function() {
                if ($(this).val().trim().length < 1) {
                    $(this).addClass("is-invalid");
                    isValid = false;
                }
            });
            return isValid;
        }

        function setSendMailBtnLoading() {
            $("#signup-btn-spinner").removeClass("d-none");
            $("#signup-btn-status").text("Working...");
            signupForm.prop('disabled', true).blur();
            $("#signup-btn-icon").addClass("d-none");
        }

        function setSendMailBtnReady() {
            $("#signup-btn-spinner").addClass("d-none");
            $("#signup-btn-status").text("Sign Up!");
            signupForm.prop('disabled', false);
            $("#signup-btn-icon").removeClass("d-none");
        }
    </script>
</content>
</body>
</html>
