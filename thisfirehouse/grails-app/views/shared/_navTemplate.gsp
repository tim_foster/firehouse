<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <div class="container">
        <a class="navbar-brand" href="/">ThisFirehouse</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item" id="nav-home">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item" id="nav-firehouses">
                    <a class="nav-link" href="/firehouse">Firehouses</a>
                </li>
                <li class="nav-item" id="nav-contact">
                    <a class="nav-link" href="/contact">Contact</a>
                </li>
                <li class="nav-item" id="nav-about">
                    <a class="nav-link" href="#">About</a>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item" id="nav-login">
                    <a class="nav-link" href="/login">Login</a>
                </li>
                <li class="nav-item" id="nav-signup">
                    <a class="nav-link" href="/signup">Signup</a>
                </li>
            </ul>
        </div>
    </div>
</nav>