<footer class="footer mt-auto">
    <div class="breakout-dark text-white pt-4 pb-4 text-center">
        <div class="container">
            <div class="row mb-2">
                <div class="col">
                    <h3 class="font-weight-bold">Connect with us on these platforms</h3>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <i class="fab fa-instagram-square fa-3x mr-3"></i>
                    <i class="fab fa-facebook-square fa-3x mr-3"></i>
                    <i class="fab fa-youtube-square fa-3x mr-3"></i>
                    <i class="fab fa-twitter-square fa-3x"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="breakout-light-dark text-white pt-4 pb-4 text-center">
        <div class="container blog-footer py-3 text-center">
            <p class="my-0">~ Ignite the fire ~</p>
            <p class="my-0">
                <a href="#">Back to top</a>&nbsp;|&nbsp;
                <sec:ifNotLoggedIn><a href="/login">Login</a></sec:ifNotLoggedIn>
                <sec:ifLoggedIn>
                    <g:link controller='logout'>Logout</g:link> &nbsp;|&nbsp;
                    <g:link controller='admin'>Admin</g:link>
                </sec:ifLoggedIn>
            </p>
        </div>
    </div>
</footer>