<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Firehouse</title>
</head>
<body>
<main role="main" class="mt-4">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>${this.firehouse?.name}</h1>
                <p>${this.firehouse?.description}</p>
                <p>Date Created: ${this.firehouse?.dateCreated}</p>
                <p>Last Updated: ${this.firehouse?.lastUpdated}</p>
                <p>Permalink: <a href="${this.firehouse?.permalink}">${this.firehouse?.permalink}</a></p>
                <p>Likes: ${this.firehouse?.likes}</p>
                <p>Embers: ${this.firehouse?.embers?.count}</p>
%{--                static hasMany = [stokers: User, embers: User, tags: Tag]--}%
            </div>
        </div>
    </div>
</main>

<content tag="footScripts">
    <script>
        $("#nav-firehouses").addClass("active");
        $("#nav-firehouses > a").append(" <span class=\"sr-only\">(current)</span>")
    </script>
</content>
</body>
</html>
