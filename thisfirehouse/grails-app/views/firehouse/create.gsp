<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Firehouse</title>
</head>
<body>
<main role="main" class="mt-4">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>Start a Firehouse</h1>
            </div>
        </div>
    </div>
</main>

<content tag="footScripts">
    <script>
        $("#nav-firehouses").addClass("active");
        $("#nav-firehouses > a").append(" <span class=\"sr-only\">(current)</span>")
    </script>
</content>
</body>
</html>
