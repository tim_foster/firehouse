<!doctype html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Tim & Caitlin Foster">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        ThisFirehouse ~ <g:layoutTitle default=""/>
    </title>
    <asset:stylesheet src="application.css"/>
    <g:layoutHead/>
</head>

<body class="d-flex flex-column h-100">
<header>
    <g:render template="/shared/navTemplate" model="[]" />
</header>
<g:layoutBody/>
<asset:javascript src="application.js"/>

<!-- Custom placeholder for page scripts -->
<g:ifPageProperty name="page.footScripts">
    <g:pageProperty name="page.footScripts" />
</g:ifPageProperty>
</body>
</html>
